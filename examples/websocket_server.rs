use std::ffi::CString;
use websocket_server::annotation::CAnnotation;
use websocket_server::{
    join_server_threads, set_resolution, set_zoom, start_websocket_server, Callbacks,
};

#[no_mangle]
extern "C" fn on_connect() {}
#[no_mangle]
extern "C" fn on_disconnect() {}
#[no_mangle]
extern "C" fn on_client(_: *const i8) {}
#[no_mangle]
extern "C" fn on_annotation_create(_: CAnnotation) {}
#[no_mangle]
extern "C" fn on_annotation_update(_: CAnnotation) {}
#[no_mangle]
extern "C" fn on_annotation_delete(_: u32) {}

fn main() {
    let callbacks = Callbacks {
        on_connect,
        on_disconnect,
        on_client,
        on_annotation_create,
        on_annotation_update,
        on_annotation_delete,
    };

    let session = start_websocket_server(
        CString::new("Test Websocket Server").unwrap().as_ptr(),
        3012,
        callbacks,
    );

    let session = set_zoom(session, 2.0);
    let session = set_resolution(session, 10, 10);

    join_server_threads(session);
}
