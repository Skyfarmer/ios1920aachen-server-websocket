extern crate protoc_rust;

use protoc_rust::Customize;

fn main() {
    protoc_rust::run(protoc_rust::Args {
        out_dir: "src/protogen",
        input: &["protofiles/streamprotocol.proto"],
        includes: &["src/protogen", "protofiles"],
        customize: Customize {
            ..Default::default()
        },
    })
    .expect("protoc");
}
