# WebSocket Server

Annotation Stream C Library

## Requirements

* protobuf
* rust

### Mac OS

```bash
brew install protobuf
brew install rustup
rustup-init
```

## Usage

1. use `git clone --recursive [repository]`
2. build the server with `./scripts/build.sh`
3. start the server with `./scripts/start.sh`

websocket server is listening on port 3012 now