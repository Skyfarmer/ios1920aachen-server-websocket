use std::convert::TryInto;
use std::sync::{Arc, Mutex};

use crate::annotation::CAnnotation;
use crate::message::*;
use crate::protogen::streamprotocol::*;
use crate::{ServerState, SAVE_ANNOTATIONS};
use std::ffi::CString;

use std::fs::File;
use std::io::prelude::*;

// each connection is represented by one of this
pub struct Connection {
    pub socket: ws::Sender,
    pub server: Arc<Mutex<ServerState>>,
}

// implement server events for connection
impl ws::Handler for Connection {
    // on new connection
    fn on_open(&mut self, _: ws::Handshake) -> ws::Result<()> {
        // add socket to socket list for broadcast
        self.server
            .lock()
            .unwrap()
            .sockets
            .push(self.socket.clone());

        // update client counter
        println!(
            "client connected! connections: {}",
            self.server.lock().unwrap().sockets.iter().count()
        );

        // fire C callback
        (self.server.lock().unwrap().callbacks.on_connect)();

        Ok(())
    }

    // on message
    fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
        // convert raw bytes into Protobuf Object
        match msg.into_data().try_into()? {
            Message::Handshake(client_handshake) => {
                println!(
                    "Handshake received from '{}'. ",
                    client_handshake.get_name()
                );

                // return server handshake
                let mut server_handshake = Handshake::new();
                server_handshake.set_name(self.server.lock().unwrap().config.server_name.to_owned());
                server_handshake
                    .set_heartbeat_interval(self.server.lock().unwrap().config.heartbeat_interval);

                self.send(Message::Handshake(server_handshake))?;

                // return current microscope resolution
                let mut microscope_resolution = MicroscopeResolution::new();
                microscope_resolution
                    .set_width(self.server.lock().unwrap().config.microscope_resolution.0 as u32);
                microscope_resolution
                    .set_height(self.server.lock().unwrap().config.microscope_resolution.1 as u32);

                self.send(Message::MicroscopeResolution(microscope_resolution))?;

                // return current video feed
                let mut videofeed_config = VideoFeedConfiguration::new();
                videofeed_config.set_field_type(VideoFeedConfiguration_Type::URI);
                videofeed_config.set_source(self.server.lock().unwrap().config.stream_url.clone().to_owned());

                self.send(Message::VideoFeedConfiguration(videofeed_config))?;

                // return current zoom
                let mut microscope_zoom = MicroscopeZoom::new();
                microscope_zoom
                    .set_factor(self.server.lock().unwrap().config.microscope_zoom);

                self.send(Message::MicroscopeZoom(microscope_zoom))?;

                // fire callback if new client is connected (with name of client)
                (self.server.lock().unwrap().callbacks.on_client)(
                    CString::new(client_handshake.name).unwrap().as_ptr(),
                );

                Ok(())
            }
            // on annotation action
            Message::Annotation(annotation) => {
                /*if SAVE_ANNOTATIONS {
                    let mut file = File::create("annotations.png")?;
                    file.write_all(&annotation.data.clone())?;

                    let mut file = File::create("annotations.svg")?;
                    file.write_all(&annotation.data.clone())?;
                }*/

                match annotation.action {
                    Annotation_Action::CREATE => {
                        println!("annotation {} created", annotation.get_id());

                        // add created annotation to annotations list
                        self.server
                            .lock()
                            .unwrap()
                            .annotations
                            .push(annotation.clone());

                        // fire C callback
                        (self.server.lock().unwrap().callbacks.on_annotation_create)(
                            annotation.clone().into(),
                        );
                    }
                    Annotation_Action::UPDATE => {
                        println!("annotation {} updated", annotation.get_id());

                        let mut server = self.server.lock().unwrap();

                        server.annotations = server
                            .annotations
                            .iter()
                            .map(|element| {
                                let mut cloned_element = element.clone();
                                // only update if id matches
                                if annotation.get_id() != element.id {
                                    return cloned_element;
                                }

                                /*// update image data
                                if annotation.get_data().iter().count() > 0 {
                                    cloned_element.set_data(annotation.get_data().to_vec());
                                }

                                // update coordinates if exists
                                if annotation.has_coordinates() {
                                    cloned_element.pos_x = annotation.get_coordinates().get_pos_x();
                                    cloned_element.pos_y = annotation.get_coordinates().get_pos_y();
                                }

                                // update timeout if set
                                if annotation.get_timeout() > 0 {
                                    cloned_element.timeout = if annotation.get_timeout() == 0 {
                                        -1
                                    } else {
                                        annotation.get_timeout() as i32
                                    };
                                }*/

                                // fire c callback
                                (server.callbacks.on_annotation_update)(cloned_element.clone().into());

                                return cloned_element;
                            })
                            .collect();

                        drop(server);
                    }
                    Annotation_Action::DELETE => {
                        println!("annotation {} deleted", annotation.id);

                        // remove deleted annotation
                        self.server
                            .lock()
                            .unwrap()
                            .annotations
                            .retain(|a| a.id != annotation.id);

                        // fire c callback
                        (self.server.lock().unwrap().callbacks.on_annotation_delete)(annotation.id);
                    }
                };

                Ok(())
            }
            Message::ClearAll(_) => {
                let mut server = self.server.lock().unwrap();

                // fire all callbacks and remove all previous annotations
                for annotation in server.annotations.clone() {
                    (server.callbacks.on_annotation_delete)(annotation.id);
                }

                // delete all annotations from server state
                server.annotations = Vec::new();

                drop(server);

                Ok(())
            }
            // currently there is no annotation error by the client
            Message::AnnotationError(_) => Ok(()),
            // heartbeat is accepted but ignored
            Message::Heartbeat(_) => Ok(()),
            // unsupported message received
            _ => {
                println!("unsupported message received");

                self.socket.close(ws::CloseCode::Error)
            }
        }
    }

    // on connection close
    fn on_close(&mut self, _: ws::CloseCode, _: &str) {
        // remove this socket from connection list
        self.server
            .lock()
            .unwrap()
            .sockets
            .retain(|element| &self.socket != element);

        // fire disconnect C callback
        (self.server.lock().unwrap().callbacks.on_disconnect)();

        println!(
            "client disconnected! connections: {}",
            self.server.lock().unwrap().sockets.iter().count()
        );
    }
}

impl Connection {
    // send alias for Message Object
    fn send(&self, msg: Message) -> ws::Result<()> {
        let data: Vec<u8> = msg.try_into().unwrap();

        self.socket.send(data)
    }
}
