extern crate ws;

use std::convert::TryInto;
use std::ffi::CStr;
use std::os::raw::c_char;
use std::sync::{Arc, Mutex};
use std::thread;
use std::thread::{sleep, JoinHandle};
use std::time::Duration;

pub mod annotation;
pub mod connection;
pub mod error;
pub mod message;

mod protogen;

use annotation::CAnnotation;
use connection::Connection;
use message::Message;
use protogen::streamprotocol::*;

pub const SAVE_ANNOTATIONS: bool = true;

/// this struct is used to store the current server configuration
pub struct ServerConfig {
    server_name: String,
    heartbeat_interval: u32,
    stream_url: String,
    microscope_resolution: (u32, u32),
    microscope_zoom: f64,
}

/// this struct stores all C callbacks not notify the library caller
#[repr(C)]
pub struct Callbacks {
    pub on_connect: extern "C" fn() -> (),
    pub on_disconnect: extern "C" fn() -> (),
    pub on_client: extern "C" fn(*const i8) -> (),
    pub on_annotation_create: extern "C" fn(CAnnotation) -> (),
    pub on_annotation_update: extern "C" fn(CAnnotation) -> (),
    pub on_annotation_delete: extern "C" fn(u32) -> (),
}

/// the struct is used to store the current server state
pub struct ServerState {
    pub sockets: Vec<ws::Sender>,
    pub annotations: Vec<Annotation>,
    pub config: ServerConfig,
    pub callbacks: Callbacks,
}

impl ServerState {
    /// sends a Message to all connected clients
    /// msg: Protobuf Message
    fn broadcast(&self, msg: Message) -> ws::Result<()> {
        let data: Vec<u8> = msg.try_into().unwrap();

        for socket in &self.sockets {
            if let Err(error) = socket.send(data.clone()) {
                println!("error during broadcast {}", error)
            };
        }

        Ok(())
    }
}

/// Reference which can be passed to C functions to refer to the current server
pub struct Session {
    server: Arc<Mutex<ServerState>>,
    websocket_thread: JoinHandle<()>,
    heartbeat_thread: JoinHandle<()>,
    cleanup_thread: JoinHandle<()>,
}

/// starts the websocket server
/// server_name: C string which is sent to the client during handshake
/// port: the port which the websocket server listens on
/// callbacks: C callbacks which notify the caller
#[no_mangle]
pub extern "C" fn start_websocket_server(
    server_name: *const c_char,
    port: u32,
    callbacks: Callbacks,
) -> *mut Session {
    // init ServerState (Arc Mutex for multithreading)
    let server = Arc::new(Mutex::new(ServerState {
        sockets: Vec::<ws::Sender>::new(),
        annotations: Vec::<Annotation>::new(),
        config: unsafe {
            ServerConfig {
                server_name: CStr::from_ptr(server_name)
                    .to_string_lossy()
                    .to_owned()
                    .to_string(),
                heartbeat_interval: 5,
                stream_url: String::from("https://aachen.pink.gg/Kung43.mp4"),
                //stream_url: String::from("rtsp://192.168.129.108:8554/camera0"),
                microscope_resolution: (1920, 1080),
                microscope_zoom: 1.0,
            }
        },
        callbacks,
    }));

    println!(
        "server {} starting...",
        server.lock().unwrap().config.server_name
    );

    let thread_server = server.clone();
    // websocket server thread
    let websocket_thread = thread::Builder::new()
        .name("websocket_server".to_owned())
        .spawn(move || {
            let thread_server = thread_server.clone();

            println!("websocket server listening on {}", port);

            // start websocket server
            ws::Builder::new()
                // server settings
                .with_settings(ws::Settings {
                    panic_on_internal: false,
                    ..ws::Settings::default()
                })
                // create a connection for each incoming socket
                .build(move |socket: ws::Sender| {
                    let local_server = thread_server.clone();
                    Connection {
                        socket,
                        server: local_server.clone(),
                    }
                })
                .unwrap()
                .listen(String::from(format!("0.0.0.0:{}", port)).as_str())
                .unwrap();
        })
        .unwrap();

    // websocket heartbeat thread
    let thread_server = server.clone();
    let heartbeat_thread = thread::Builder::new()
        .name("websocket_heartbeat".to_owned())
        .spawn(move || loop {
            let thread_server = thread_server.clone();

            let interval = thread_server.lock().unwrap().config.heartbeat_interval;
            // sleep for heartbeat interval
            sleep(Duration::from_millis(interval as u64 * 1000, ));

            // broadcast heartbeat to all connected clients
            thread_server
                .lock()
                .unwrap()
                .broadcast(Message::Heartbeat(Heartbeat::new()))
                .unwrap();
        })
        .unwrap();

    // annotation cleanup thread
    let thread_server = server.clone();
    let cleanup_thread = thread::Builder::new()
        .name("annotation_cleanup".to_owned())
        .spawn(move || {
            let thread_server = thread_server.clone();

            // infinite loop
            loop {
                // clean annotations every second
                sleep(Duration::from_millis(1000));

                let mut server = thread_server.lock().unwrap();

                // reduce the timeout of every annotation by 1
                server.annotations = server
                    .annotations
                    .iter()
                    .map(|annotation| {
                        let mut cloned_annotation = annotation.clone();

                        if annotation.timeout > 0 {
                            cloned_annotation.timeout -= 1;
                        }

                        return cloned_annotation;
                    })
                    .collect();

                // remove all expired annotations
                server
                    .annotations
                    .retain(|annotation| annotation.timeout != 0);

                drop(server);
            }
        })
        .unwrap();

    // return pointer on session (in heap)
    return Box::into_raw(Box::new(Session {
        server,
        websocket_thread,
        heartbeat_thread,
        cleanup_thread,
    }));
}

/// updates zoom of the microscope
#[no_mangle]
pub extern "C" fn set_zoom(session: *mut Session, factor: f64) -> *mut Session {
    let session = unsafe { Box::from_raw(session) };
    let mut server = session.server.lock().unwrap();

    // update server config
    server.config.microscope_zoom = factor;

    // init protobuf object
    let mut protobuf_message = MicroscopeZoom::new();
    protobuf_message.set_factor(factor);

    server
        .broadcast(Message::MicroscopeZoom(protobuf_message))
        .unwrap();

    println!("updated zoom level: {}", factor);

    // drop borrowed var (from session)
    drop(server);
    // convert session back to raw pointer to not free the allocated memory
    return Box::into_raw(session);
}

/// updates microscope resolution
#[no_mangle]
pub extern "C" fn set_resolution(session: *mut Session, width: u32, height: u32) -> *mut Session {
    let session = unsafe { Box::from_raw(session) };
    let mut server = session.server.lock().unwrap();

    // update server config
    server.config.microscope_resolution = (width, height);

    // init protobuf object
    let mut protobuf_message = MicroscopeResolution::new();
    protobuf_message.set_width(width as u32);
    protobuf_message.set_height(height as u32);

    // notify all connected clients that the resolution has changed
    server
        .broadcast(Message::MicroscopeResolution(protobuf_message))
        .unwrap();

    println!("updated microscope resolution: {} x {}", width, height);

    // drop borrowed var (from session)
    drop(server);
    // convert session back to raw pointer to not free the allocated memory
    return Box::into_raw(session);
}

/// updates stream url
#[no_mangle]
pub extern "C" fn set_stream_url(session: *mut Session, url: *const c_char) -> *mut Session {
    let session = unsafe { Box::from_raw(session) };
    let mut server = session.server.lock().unwrap();

    // update server config
    server.config.stream_url =
        unsafe { CStr::from_ptr(url).to_string_lossy().to_owned().to_string() };

    // init protobuf object
    let mut protobuf_message = VideoFeedConfiguration::new();
    protobuf_message.set_field_type(VideoFeedConfiguration_Type::URI);
    protobuf_message.set_source(server.config.stream_url.clone());

    // notify all connected clients that the url has changed
    server
        .broadcast(Message::VideoFeedConfiguration(protobuf_message))
        .unwrap();

    println!("updated stream url: {}", server.config.stream_url);

    // drop borrowed var (from session)
    drop(server);
    // convert session back to raw pointer to not free the allocated memory
    return Box::into_raw(session);
}

/// waits for all threads to terminate
#[no_mangle]
pub extern "C" fn join_server_threads(session: *mut Session) {
    let session = unsafe { Box::from_raw(session) };

    session.websocket_thread.join().unwrap();
    session.heartbeat_thread.join().unwrap();
    session.cleanup_thread.join().unwrap();

    println!("all threads terminated");
}
