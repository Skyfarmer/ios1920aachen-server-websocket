use std::error::Error;
use std::fmt::{Debug, Formatter};

/// Standard Error which can be converted to Protobuf Error and Websocket Error.
#[derive(Debug)]
pub enum ServerError {
    Websocket(ws::Error),
    Protobuf(protobuf::error::ProtobufError),
    Parsing(String),
}

impl Error for ServerError {}

impl std::fmt::Display for ServerError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        use std::fmt::Display;

        match self {
            Self::Websocket(e) => Display::fmt(e, f),
            Self::Protobuf(e) => Display::fmt(e, f),
            Self::Parsing(e) => write!(f, "{}", e),
        }
    }
}

impl From<protobuf::error::ProtobufError> for ServerError {
    fn from(err: protobuf::error::ProtobufError) -> Self {
        ServerError::Protobuf(err)
    }
}

impl From<ws::Error> for ServerError {
    fn from(err: ws::Error) -> Self {
        ServerError::Websocket(err)
    }
}

impl From<ServerError> for ws::Error {
    fn from(error: ServerError) -> Self {
        match error {
            ServerError::Websocket(e) => e,
            ServerError::Protobuf(e) => {
                ws::Error::new(ws::ErrorKind::Custom(Box::new(e)), "Protobuf")
            }
            e => ws::Error::new(ws::ErrorKind::Custom(Box::new(e)), "Internal"),
        }
    }
}
