use std::convert::TryFrom;

use crate::error::ServerError;
use crate::protogen::streamprotocol::*;
use protobuf::Message as ProtobufMessage;

/// This enum wraps and identify protobuf messages
pub enum Message {
    Handshake(Handshake),
    Heartbeat(Heartbeat),
    AnnotationError(AnnotationError),
    Annotation(Annotation),
    MicroscopeResolution(MicroscopeResolution),
    VideoFeedConfiguration(VideoFeedConfiguration),
    MicroscopeZoom(MicroscopeZoom),
    ClearAll(ClearAll),
}

/// Trait to cast bytearray into Message automatically
impl TryFrom<Vec<u8>> for Message {
    type Error = ServerError;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        // last byte identifies the message type.
        // remove it and iterate over all possibilities
        match value.last() {
            Some(1) => Ok(Message::Handshake(protobuf::parse_from_bytes::<Handshake>(
                &value[..value.len() - 1],
            )?)),
            Some(2) => Ok(Message::Heartbeat(protobuf::parse_from_bytes::<Heartbeat>(
                &value[..value.len() - 1],
            )?)),
            Some(3) => Ok(Message::AnnotationError(protobuf::parse_from_bytes::<
                AnnotationError,
            >(
                &value[..value.len() - 1]
            )?)),
            Some(4) => Ok(Message::Annotation(
                protobuf::parse_from_bytes::<Annotation>(&value[..value.len() - 1])?,
            )),
            Some(5) => Ok(Message::MicroscopeResolution(protobuf::parse_from_bytes::<
                MicroscopeResolution,
            >(
                &value[..value.len() - 1]
            )?)),
            Some(7) => Ok(Message::MicroscopeZoom(protobuf::parse_from_bytes::<
                MicroscopeZoom,
            >(
                &value[..value.len() - 1]
            )?)),
            Some(8) => Ok(Message::ClearAll(protobuf::parse_from_bytes::<ClearAll>(
                &value[..value.len() - 1],
            )?)),
            Some(x) => Err(ServerError::Parsing(format!("Unknown type {}", x))),
            None => Err(ServerError::Parsing("Empty data array".to_owned()))
        }
    }
}

// Trait to convert Message to bytearray
impl TryFrom<Message> for Vec<u8> {
    type Error = ServerError;

    fn try_from(value: Message) -> Result<Vec<u8>, Self::Error> {
        let (mut data, message_id) = match value {
            Message::Handshake(protobuf_object) => (protobuf_object.write_to_bytes()?, 1),
            Message::Heartbeat(protobuf_object) => (protobuf_object.write_to_bytes()?, 2),
            Message::AnnotationError(protobuf_object) => (protobuf_object.write_to_bytes()?, 3),
            Message::Annotation(protobuf_object) => (protobuf_object.write_to_bytes()?, 4),
            Message::MicroscopeResolution(protobuf_object) => {
                (protobuf_object.write_to_bytes()?, 5)
            }
            Message::VideoFeedConfiguration(protobuf_object) => {
                (protobuf_object.write_to_bytes()?, 6)
            }
            Message::MicroscopeZoom(protobuf_object) => (protobuf_object.write_to_bytes()?, 7),
            Message::ClearAll(protobuf_object) => (protobuf_object.write_to_bytes()?, 8),
        };

        // last byte represents the message type
        data.push(message_id);

        Ok(data)
    }
}
