use std::alloc::{alloc, dealloc, Layout};
use std::mem;
use std::ptr;
use crate::protogen::streamprotocol::Annotation;
use std::os::raw::c_uint;

/// C compatible Annotation object which can be cloned
#[repr(C)]
pub struct CAnnotation {
    pub id: c_uint,
}

impl Clone for CAnnotation {
    /// Annotations should be cloneable
    fn clone(&self) -> Self {
        return CAnnotation::new(
            self.id,
        );
    }
}

impl Into<CAnnotation> for Annotation {
    fn into(self) -> CAnnotation {
        CAnnotation { id: self.get_id() }
    }
}

// free memory if ownership is dropped
impl Drop for CAnnotation {
    fn drop(&mut self) {
        self.free_data();
    }
}

// allow to transfer struct between threads
unsafe impl Send for CAnnotation {}

impl CAnnotation {
    /// static fn new which allocates data for Vec to be C compatible
    pub fn new(id: u32) -> CAnnotation {
        let mut element = CAnnotation {
            id
        };

        return element;
    }

    /// gets data as Vector
    pub fn get_data(&self) -> Vec<u8> {
        /*unsafe {
            let mut data: Vec<u8> = Vec::new();

            for i in 0..self.data_length {
                data.push(self.data_pointer.offset(i as isize).read())
            }

            return data;
        }*/

        return Vec::new()
    }

    /// sets data as Vector
    pub fn set_data(&mut self, data: Vec<u8>) {
        /*unsafe {
            // free data to overwrite it
            self.free_data();
            // byte array length
            let size = data.iter().count();
            // memory layout
            let layout = Layout::from_size_align(size, Self::memory_align()).unwrap();
            // allocate memory
            let pointer: *mut u8 = mem::transmute(alloc(layout));

            // write bytes to memory
            for (i, byte) in data.iter().enumerate() {
                ptr::write(pointer.offset(i as isize), byte.clone());
            }

            // update fields
            self.data_pointer = pointer;
            self.data_length = size as u32;
        }*/
    }

    /// free allocated data
    pub fn free_data(&mut self) {
        /*// do not need to free memory if data is not set yet (null pointer)
        if self.data_pointer.is_null() {
            return;
        }

        unsafe {
            // memory layout
            let layout =
                Layout::from_size_align(self.data_length as usize, Self::memory_align()).unwrap();
            // free data
            dealloc(self.data_pointer, layout);

            // set data to null pointer again
            self.data_pointer = ptr::null_mut();
            self.data_length = 0;
        }*/
    }

    // number should be big enough for a couple of kbs (image data)
    fn memory_align() -> usize {
        return 4 * 1024;
    }
}
