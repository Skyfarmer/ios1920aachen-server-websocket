extern "C" {
    struct Session;

    struct Annotation {
        unsigned int id;
        int pos_x;
        int pos_y;
        int timeout;
        void* data_pointer;
        unsigned int data_length;
    };

    struct Callbacks {
        void (*on_connect)();
        void (*on_disconnect)();
        void (*on_client)(char*);

        void (*on_annotation_create)(Annotation);
        void (*on_annotation_update)(Annotation);
        void (*on_annotation_delete)(unsigned int);
    };

    Session* start_websocket_server(const char*, unsigned int, Callbacks);
    Session* set_zoom(Session*, double);
    Session* set_resolution(Session*, unsigned int, unsigned int);
    Session* set_stream_url(Session*, const char*);
    void join_server_threads(Session*);
}
