#!/bin/bash
cd "${BASH_SOURCE%/*}/../"
cargo build
cp ./target/debug/libwebsocket_server.dylib ./scripts/

# Remove CMake files if they exists in scripts directory
rm -fv ./scripts/CMakeCache.txt
rm -rfv ./scripts/CMakeFiles

mkdir -p ./scripts/build/
cd ./scripts/build/

cmake ..
make
