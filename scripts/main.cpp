#include <iostream>

#include "libwebsocket_server.hpp"
//#include "bonjour/src/ZCFGService.h"
#include "bonjour/src/main.h"

// start bonjour service
int start_bonjour() {
    ZCFGServiceInformation servInfo("microscope", "_icaras-service._tcp.", "local", 3012);
    return ZCFGService(servInfo).runService();
}

// callbacks
void on_connect() { }
void on_disconnect() { }
void on_client(char* name) { }
void on_annotation_create(Annotation annotation) { }
void on_annotation_update(Annotation annotation) { }
void on_annotation_delete(unsigned int id) { }

int main(int argc, char **argv) {
    // pack callbacks into struct
    Callbacks callbacks = {
        .on_connect = &on_connect,
        .on_disconnect = &on_disconnect,
        .on_client = &on_client,
        .on_annotation_create = &on_annotation_create,
        .on_annotation_update = &on_annotation_update,
        .on_annotation_delete = &on_annotation_delete,
    };

    // start websocket sercer
    Session* session = start_websocket_server(std::string("C++ Server").c_str(), 3012, callbacks);

    session = set_zoom(session, 8.0);
    session = set_resolution(session, 1920, 1080);

    // start bonhour service
    start_bonjour();
    // idle
    join_server_threads(session);

    return 0;
}
